#Havamal Alexa Skill
A simple [AWS Lambda](http://aws.amazon.com/lambda) function that demonstrates how to write a skill for the Amazon Echo using the Alexa SDK.

##Description
This is a simple skill that provides a random "Norse Havamal" from a pre-defined list of entries.

##History Lesson
Hávamál ("sayings of the high one") is presented as a single poem in the Codex Regius, a collection of Old Norse poems from the Viking age. The poem, itself a combination of different poems, is largely gnomic, presenting advice for living, proper conduct and wisdom.

The verses are attributed to Odin; the implicit attribution to Odin facilitated the accretion of various mythological material also dealing with Odin.

##Source
The Elder or Poetic Edda, commonly known as Sæmund's Edda, part I: The Mythological Poems, edited and translated by Olive Bray (London: Printed for the Viking Club, 1908), pp. 61-111. (http://www.pitt.edu/~dash/havamal.html)