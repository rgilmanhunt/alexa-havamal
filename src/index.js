var APP_ID = undefined; //OPTIONAL: replace with "amzn1.echo-sdk-ams.app.[your-unique-value-here]";
var havamal = require('./sayings.js');
var havamals = havamal.sayings();


/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

/**
 * SpaceGeek is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var Fact = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
Fact.prototype = Object.create(AlexaSkill.prototype);
Fact.prototype.constructor = Fact;

Fact.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    //console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

Fact.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    //console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleNewFactRequest(response);
};

/**
 * Overridden to show that a subclass can override this function to teardown session state.
 */
Fact.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    //console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

Fact.prototype.intentHandlers = {
    "GetNewFactIntent": function (intent, session, response) {
        handleNewFactRequest(response);
    },

    "GetSpecificFactIntent": function(intent, session, response) {
    	handleSpecificFactRequest(intent, response);
    },

    "GetVersionIntent": function(intent, session, response) {
        handleVersionIntent(response);
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        response.ask("Would you like to hear a Havamal stanza?");
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    }
};

/**
 * Gets a random new fact from the list and returns to the user.
 */
function handleNewFactRequest(response) {
    // Get a random space fact from the space facts list
    var factIndex = Math.floor(Math.random() * havamals.length);
    var randomFact = havamals[factIndex];

    // Create speech output
    var speechOutput = randomFact;
    var cardTitle = "Your Saying";
    response.tellWithCard(speechOutput, cardTitle, speechOutput);
}

function handleSpecificFactRequest(intent, response) {


    // get a specific one
    var factIndex=0;
    // slots.number-1
    factIndex =  intent.slots.Specific.value - 1;
    var saying = havamals[factIndex];

    if(saying) {
        var speechOutput = saying;
        var cardTitle = "Havamal Number " + (factIndex+1);
    }
    else {
        var speechOutput = "No Wisdom found for that number.";
        var cardTitle = "Unknown Wisdom";
    }

    // Create speech output
    response.tellWithCard(speechOutput, cardTitle, speechOutput);
}

function handleVersionIntent(response) {
    var cardTitle = "Havamal Wisdom Version";
    var cardContent = "0.0.3";

    var speechOutput = {
        speech: "<speak>I am version zero dot zero dot three</speak>",
        type: AlexaSkill.speechOutputType.SSML
    };

    response.tellWithCard(speechOutput, cardTitle, cardContent);


}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    var fact = new Fact();
    fact.execute(event, context);
};

